﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController(IService<Flight> flightService, IMapper mapper, IService<Passenger> personService)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPost("/flights/{flightId:long}/addPassenger/{passengerId}")]
    [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> AddPassengerToFlight(long flightId, string passengerId)
    {
        var passenger = await personService.FindAsync(passengerId);
        if (passenger is null)
        {
            return NotFound($"Passenger with ID '{passengerId}' not found.");
        }

        var flight = await flightService.FindAsync(flightId);
        if (flight is null)
        {
            return NotFound($"Flight with ID {flight} not found.");
        }

        flight.Result.Passengers.Add(passenger);

        await flightService.UpdateAsync(flight);

        return Ok();
    }

}